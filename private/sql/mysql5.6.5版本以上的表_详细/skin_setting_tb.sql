CREATE TABLE `skin_setting_tb`(
   `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
   `skin_id` INT UNSIGNED NOT NULL COMMENT '皮肤ID,自定义0',
   `lock_background` ENUM('1', '2') NOT NULL DEFAULT '2' COMMENT '锁定皮肤背景(背景图不随页面滚动),1=滚动,2=不滚动',
   `setting_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '皮肤设置时间',
   PRIMARY KEY `userId_pk` (`user_id`)
 ) ENGINE=MYISAM COMMENT='皮肤设置表' CHARSET=utf8 COLLATE=utf8_general_ci;